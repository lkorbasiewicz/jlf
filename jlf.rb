require 'pp'
require 'json'
require 'pathname'
verbose = 1
path = Pathname.new(File.expand_path(ARGV.shift))
if ARGV[0]
  filter = ARGV.shift
else
  filter = ''
end
duration_ms = ARGV.shift.to_f
i = 0
File.foreach(path) do |logline|
  if logline.include?("duration_ms") && logline.include?(filter)
    if JSON.parse(logline)['duration_ms'] > duration_ms
      if verbose == 1
        pp JSON.parse(logline)
      end
      i += 1
    end
  end
end
puts "\nThere are " + i.to_s + " " + filter + " requests longer than " + (duration_ms / 1000).to_s + " seconds"
