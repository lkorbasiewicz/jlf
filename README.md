# JSON LOG FILTER

Small script to filter keywords from JSON log files, identify and count long requests

## Usage

The command takes up to 3 parameters (in this order):
1. JSON log file name / path (mandatory)
1. filter string (if not given, it will display all entries)
1. time in miliseconds (if not given, it will display all entries with time_ms > 0)

## Examples

In this example we want to see all requests that contain the word "badgateway" and are longer than 45000ms

```
$ ruby jlf.rb example.log badgateway 45000
{"correlation_id"=>"0101FFAACCDD99BB",
 "duration_ms"=>45143,
 "error"=>"badgateway: failed to receive response: EOF",
 "level"=>"error",
 "method"=>"GET",
 "msg"=>"",
 "time"=>"2021-11-30T12:56:45Z",
 "uri"=>
  "/search/count?group_id=1337&project_id=&repository_ref=&scope=epics&search=accountservice&snippets=false"}

There are 1 badgateway requests longer than 45.0 seconds
```
## Configuration

Change `verbose = 1` to `0` if you don't want the script to print out all values, only count them